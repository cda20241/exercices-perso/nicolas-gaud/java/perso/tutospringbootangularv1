package net.javaguides.springbootbackend.model;

import jakarta.persistence.*;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nickname")
    private String nickName;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

//    @Transient ==> in this case userTypeId = 0
    @Column(name = "user_type_id")
    private int userTypeid;

    public User() {
    }

    public User(int id, String nickName, String email, String password, int userTypeid) {
        this.id = id;
        this.nickName = nickName;
        this.email = email;
        this.password = password;
        this.userTypeid = userTypeid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserTypeid() {
        return userTypeid;
    }

    public void setUserTypeid(int userTypeid) {
        this.userTypeid = userTypeid;
    }
}
